# ISIS-practical-session

Practical Session for Federated Learning with Fed-BioMed software


## Courses: timetable


![i](./assets/TimeTable-FL.png)

## Getting started

First, please clone the following repository on your device (or VM): open a terminal

```bash

git clone https://gitlab.inria.fr/isis-federated-learning/isis-practical-session.git

cd ISIS-practical-session

ISIS_DIR=$PWD

echo $ISIS_DIR
>>> /path/to/ISIS-practical-session
```


Now copy the files into the `fedbiomed/notebook` folder

First go to your fedbiomed installation folder (for VM, should be in `~/fedbiomed`)



```bash
FEDBIOMED_DIR=$PWD

echo $FEDBIOMED_DIR
>>> path/to/fedbiomed
```


```bash
cp -R  $ISIS_DIR/notebooks/* $FEDBIOMED_DIR/notebooks


```


## Start the `Network`

Run the Network using the following comand:

```bash
cd $FEDBIOMED_DIR

./scripts/fedbiomed_run network

```
## Set up the Nodes

Go to `fedbiomed` repo

```bash

cd $FEDBIOMED_DIR

./scripts/fedbiomed_run node config isis_client_1 add
```

and then select option `4` to load images. Please provide a description and a appropriate tag. Please be careful when specify the path if folder has several clients, for some paths consider only a subset of the dataset

Create other nodes by naming the configuration differently, for instance `isis_client_2`:


```bash

cd $FEDBIOMED_DIR

./scripts/fedbiomed_run node config isis_client_2 add
```

## Remove a dataset

If you want to remove a dataset (bad path file or tag for instance), you may run:

```bash
./scripts/fedbimed_run node config isis_client_1 delete

```

## Start the Nodes

In order to connect the Nodes to the network, you may run the `start` option

```bash
./scripts/fedbimed_run node config isis_client_1 start
```

Once `Nodes`, `Network` and `Researcher` have been launched, you can freely run the `Researcher` notebook cells

## Run the Pratical Sessions

```bash

./scripts/fedbiomed_run researcher start
```

Then a notebook opens: it is the `Researcher` interface

## Datasets

[Datasets can be found here:](https://mybox.inria.fr/library/d64b6564-0bb7-4f07-a342-6a737367f5e7/Fed-BioMed-teaching/2023-12-07-ISIS-Castres-FL-Fed-BioMed-class/practical_session)



## Troubleshooting


Sometime, for some reasons, `Nodes` crash and leave config files or model parameters files corrupted. If you happen to have the folowing error when running `Experiment.run` command: 

```bash
FB401: aggregation crashes or returns an error. Aggregation aborted due to sum of the weights is equal to 0 {}. Sample sizes received from nodes might be corrupted.\n
```

You can reset your settings by running:

```bash
source ./scripts/fedbiomed_environment clean

```

**Warning**: when running such command, you will have to set once again the `Nodes`. 

